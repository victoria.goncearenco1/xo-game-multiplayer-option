import React from "react";
import Board from "./Board";
import Boxes from "./Boxes";

const defaultBoxes = () => Array(9).fill(null);

const lines = [
  [0, 1, 2],
  [3, 4, 5],
  [6, 7, 8],
  [0, 3, 6],
  [1, 4, 7],
  [2, 5, 8],
  [0, 4, 8],
  [2, 4, 6],
];

export default class Container extends React.Component {
  constructor(props) {
    super(props);
    this.handleBoxClick = this.handleBoxClick.bind(this);
    this.handleRestart = this.handleRestart.bind(this);
    this.state = {
      boxes: defaultBoxes(),
      winner: null,
    };
  }

  componentDidUpdate(prevProps, prevState) {
    //aici sunt dependintele din useEffect
    if (
      this.state.boxes !== prevState.boxes &&
      !(
        prevState.winner === "o" ||
        prevState.winner === "x" ||
        prevState.winner === "tie"
      )
    ) {
      console.log(prevProps, "mesaj de props");
      const isComputerTurn =
        this.state.boxes.filter((box) => box !== null).length % 2 === 1;
      console.log("a intrat");

      const linesThatAre = (a, b, c) => {
        return lines.filter((boxIndexes) => {
          const boxValues = boxIndexes.map((index) => this.state.boxes[index]);
          return (
            JSON.stringify([a, b, c].sort()) ===
            JSON.stringify(boxValues.sort())
          );
        });
      };
      const emptyIndexes = this.state.boxes
        .map((box, index) => (box === null ? index : null))
        .filter((val) => val !== null);
      const playerWon = linesThatAre("x", "x", "x").length > 0;
      const computerWon = linesThatAre("o", "o", "o").length > 0;
      if (playerWon) {
        console.log("player won");
        this.setState({
          boxes: [...this.state.boxes],
          winner: "x",
        });
      }
      if (computerWon) {
        console.log("computer has won");
        this.setState({
          boxes: [...this.state.boxes],
          winner: "o",
        });
      }
      if (!emptyIndexes.length && !(computerWon || playerWon)) {
        // emptyIndexes este o matrice cu lenght 0 insemnand ca are valoarea falsey
        this.setState({
          winner: "tie",
        });
      }
      const putComputerAt = (index) => {
        let newBoxes = this.state.boxes;
        newBoxes[index] = "o";
        this.setState({
          boxes: [...newBoxes],
        });
      };
      if (isComputerTurn) {
        const winingLines = linesThatAre("o", "o", null);
        if (winingLines.length > 0) {
          const winIndex = winingLines[0].filter(
            (index) => this.state.boxes[index] === null
          )[0];
          putComputerAt(winIndex);
          return;
        }

        const linesToBlock = linesThatAre("x", "x", null);
        if (linesToBlock.length > 0) {
          const blockIndex = linesToBlock[0].filter(
            (index) => this.state.boxes[index] === null
          )[0];
          putComputerAt(blockIndex);
          return;
        }

        const linesToContinue = linesThatAre("o", null, null);
        if (linesToContinue.length > 0) {
          putComputerAt(
            linesToContinue[0].filter(
              (index) => this.state.boxes[index] === null
            )[0]
          );
          return;
        }

        const randomIndex =
          emptyIndexes[Math.ceil(Math.random() * emptyIndexes.length)];
        putComputerAt(randomIndex);
      }
    }
  }

  handleBoxClick(index, event) {
    if (event.target.innerText === "") {
      const isPlayerTurn =
        this.state.boxes.filter((box) => box !== null).length % 2 === 0;
      if (isPlayerTurn) {
        let newBoxes = this.state.boxes;
        newBoxes[index] = "x";
        this.setState({
          boxes: [...newBoxes],
        });
      }
    }
  }

  handleRestart() {
    return (
      <button className="restart" onClick={() => window.location.reload()}>
        {" "}
        Restart the game
      </button>
    );
  }

  render() {
    return (
      <main>
        <Board>
          {this.state.boxes.map((box, index) => (
            <Boxes
              x={box === "x" ? 1 : 0}
              o={box === "o" ? 1 : 0}
              onClick={this.handleBoxClick.bind(null, index)}
            />
          ))}
        </Board>
        {!!this.state.winner && this.state.winner === "x" && (
          <div className="score player">You WON!</div>
        )}
        {!!this.state.winner && this.state.winner === "o" && (
          <div className="score computer">Computer Won!</div>
        )}
        {!!this.state.winner && this.state.winner === "tie" && (
          <div className="score tie">Tie!</div>
        )}
        {this.handleRestart()}
      </main>
    );
  }
}
