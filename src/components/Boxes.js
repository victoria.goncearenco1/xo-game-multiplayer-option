import React from "react";

function Boxes(props) {
  return (
    <div className="boxes" {...props}>
      {props.x ? "x" : props.o ? "o" : ""}
    </div>
  );
}

export default Boxes;
