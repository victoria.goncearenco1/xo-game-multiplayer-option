import "./App.css";
import "./index";
import Party from "./components/Particles";

import Container from "./components/Container";

function App() {
  const particlesInit = () => {
  };

  const particlesLoaded = () => {};
  return (
    <>
      <Container />
      <Party
        id="tsparticles"
        url="http://foo.bar/particles.json"
        init={particlesInit}
        loaded={particlesLoaded}
      />
    </>
  );
}

export default App;
